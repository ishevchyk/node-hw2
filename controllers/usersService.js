const {User} = require("../models/user");
const bcrypt = require("bcryptjs");
const getUser = async (req, res) => {
  const user = await User.findById(req.user._id).select('-password')
  res.send({
    "user": {
      "_id": user._id,
      "username": user.username,
      "createdDate": user.createdDate
    }
  })
}

const deleteUser = async (req, res) => {
  await User.findByIdAndRemove(req.user._id)
  res.send({
    "message": "Success"
  })
}

const changePassword = async (req, res) => {
  let {oldPassword, newPassword} = req.body;
  let user = await User.findById(req.user._id);

  const validPassword = await bcrypt.compareSync(oldPassword, user.password);
  if (!validPassword) return res.status(400).send({
    "message": "Invalid password"
  });

  const salt = await bcrypt.genSaltSync(10);
  newPassword = await bcrypt.hashSync(newPassword, salt);

  await User.findByIdAndUpdate(req.user._id, {
    password: newPassword,
    new: true
  })

  res.send({
    "message": "Success"
  })
}

module.exports = {
  getUser,
  deleteUser,
  changePassword
}
