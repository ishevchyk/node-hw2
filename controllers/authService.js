const {User, validateUser} = require("../models/user");
const bcrypt = require("bcryptjs");

const registerUser = async (req, res) => {
  const {error} = validateUser(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({username: req.body.username});

  if (user) return res.status(400).send({
    "message": "User already exist."
  });

  const { username, password} = await req.body;
  user = new User({username, password});

  const salt = await bcrypt.genSaltSync(10);
  user.password = await bcrypt.hashSync(user.password, salt);

  await user.save()

  res.send({
    "message": "Success"
  });
}

const loginUser = async (req, res) => {
  const {error} = validateUser(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({username: req.body.username});
  if (!user) return res.status(400).send({
    "message": "Invalid username or password"
  });

  const validPassword = await bcrypt.compareSync(String(req.body.password), String(user.password));
  if (!validPassword) return res.status(400).send({
    "message": "Invalid username or password"
  });

  const token = user.generateAuthToken();

  res.header('x-auth-token', token).send({
    "message": "Success",
    "jwt_token": `${token}`
  })
}

// const validate = (req) => {
//   const schema = Joi.object({
//     username: Joi.string().min(5).max(255).required(),
//     password: Joi.string().min(5).max(255).required(),
//   });
//   return schema.validate(req)
// }

module.exports = {
  registerUser,
  loginUser
}
