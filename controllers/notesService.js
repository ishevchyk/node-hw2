const {Note, validateNote} = require('../models/note')

const getNotes = async (req, res) => {
  const userId = req.user._id;
  const notes = await Note
    .find({userId: userId})
  res.send({
    notes
  })
}

const getNoteById = async (req, res) => {
  try {
    const note = await Note.findById(req.params.id).select('-updatedAt');
    console.log(note)
    res.send( { note } )
  } catch (ex) {
    res.status(404).send({ "message": "Invalid id" })
  }
}

const createNote = async (req, res) => {
  const {error} = validateNote(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  const {text, completed} = await req.body;

  const userId = req.user._id
  const note = new Note({
    userId,
    text,
    completed
  })
  await note.save();
  res.send({
    "message": "Success"
  })
}

const deletePersonalNote = async (req, res) => {
    const note = await Note.findById(req.params.id);
    if(!note) return res.status(404).send({
      "message": "Invalid note ID"
    })

    if (JSON.stringify(note.userId) !== JSON.stringify(req.user._id)) return res.status(404).send({
      "message": "You cannot remove notes that are not yours!"
    })

    await Note.findByIdAndRemove(req.params.id)
    res.send({
      "message": "Success"
    })
}

const updatePersonalNoteById = async (req, res) => {
  const note = await Note.findById(req.params.id);
  if(!note) return res.status(404).send({
    "message": "Invalid note ID"
  })

  if (JSON.stringify(note.userId) !== JSON.stringify(req.user._id)) return res.status(404).send({
    "message": "You cannot update notes that are not yours!"})

  await Note.findByIdAndUpdate(req.params.id, {
    text: req.body.text,
    new: true })
  res.send({
    "message": "Success"
  })
}

const checkNoteById = async (req, res) => {
  try {
    const note = await Note.findById(req.params.id);

    if(note.completed) {
      await Note.findByIdAndUpdate(req.params.id, {
        $set: {
          completed: false
        }
      }, {new: true});
    } else {
      await Note.findByIdAndUpdate(req.params.id, {
        $set: {
          completed: true
        }
      }, {new: true});
    }
    res.send({
      "message": "Success"
    })
  } catch (ex) {
    res.status(404).send({ "message": "Something went wrong.. Check if you have provided a valid id!" })
  }
}

module.exports = {
  createNote,
  getNotes,
  getNoteById,
  updatePersonalNoteById,
  checkNoteById,
  deletePersonalNote
}
