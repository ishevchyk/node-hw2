const express = require('express');
const router = express.Router();
const {
  createNote, getNotes, getNoteById,
  deleteNoteById, checkNoteById,
  deletePersonalNote, updatePersonalNoteById
} = require('../controllers/notesService');
const authMiddleware = require('../middleware/authMiddleware')


router.get('/', authMiddleware, getNotes);
router.get('/:id', getNoteById);
router.post('/', authMiddleware, createNote);
router.put('/:id', authMiddleware, updatePersonalNoteById);
router.patch('/:id', checkNoteById);
router.delete('/:id', authMiddleware, deletePersonalNote);

module.exports = router;
