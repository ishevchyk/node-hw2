const express = require('express');
const router = express.Router();
const authMiddleware = require('../middleware/authMiddleware')
const {getUser, deleteUser, changePassword} = require("../controllers/usersService");

router.get('/me', authMiddleware, getUser);
router.delete('/me', authMiddleware, deleteUser);
router.patch('/me', authMiddleware, changePassword)

module.exports = router;
