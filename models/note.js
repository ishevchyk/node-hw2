const mongoose = require('mongoose');
// const {User, usersSchema} = require('../models/user');
const Joi = require("joi");

const notesSchema = new mongoose.Schema ({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  text: {
    type: String,
    required: true,
    min: 1
  }
}, {timestamps:
    {createdAt: "createdDate"}
})


const Note = mongoose.model('Note', notesSchema);

const validateNote = (note) => {
  const schema = Joi.object({
    text: Joi.string().min(0).required(),
    completed: Joi.boolean()
  });
  return schema.validate(note)
}

module.exports = {
  Note,
  validateNote
}


