const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const dotenv = require('dotenv').config();

const usersSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    minLength: 1,
    maxLength: 255
  },
  password: {
    type: String,
    required: true,
    minLength: 5,
    maxLength: 1024
  }
}, {timestamps: {
    createdAt: "createdDate"
  }})

usersSchema.methods.generateAuthToken = function () {
  return jwt.sign({_id: this._id}, dotenv.parsed.JWT_SECRET_KEY);
}

const User = mongoose.model('User', usersSchema);

const validateUser = (user) => {
  const schema = Joi.object({
    username: Joi.string().min(1).max(55).required(),
    password: Joi.string().min(1).max(255).required(),
  });
  return schema.validate(user)
}
module.exports = {
  User,
  validateUser,
  usersSchema
}
