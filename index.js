const express = require('express');
const app = express();

require('./startup/logging')(app)
require('./startup/routes')(app);
require('./startup/db')();
require('./startup/config')();
require('./startup/error')(app)

const port = process.env.PORT || 8080;
app.listen(port, () => console.log(`Listening on port: ${port}...`))
