const dotenv = require('dotenv').config();
module.exports = function () {
  if (!dotenv.parsed.JWT_SECRET_KEY) {
    console.error('FATAL ERROR: jwtSecretKey is not defined');
    process.exit(1);
  }
}
