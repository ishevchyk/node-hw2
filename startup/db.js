const mongoose = require("mongoose");
const dotenv = require('dotenv').config();
module.exports = function () {
  mongoose.connect(`mongodb+srv://${dotenv.parsed.DB_NAME}:${dotenv.parsed.DB_PASSWORD}@atlascluster.itgxvxt.mongodb.net/todoapp`)
    .then(() => console.log('Connected'))
    .catch((err) => console.log(err))
}
